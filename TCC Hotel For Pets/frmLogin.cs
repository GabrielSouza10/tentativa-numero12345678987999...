﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TCC_Hotel_For_Pets.DB.Login;
using TCC_Hotel_For_Pets.Telas.Menu_inicial;

namespace TCC_Hotel_For_Pets.Telas
{
    public partial class frmForm3 : Form
    {
        public frmForm3()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void txtCPF_TextChanged(object sender, EventArgs e)
        {

        }

        private void lblnome_Click(object sender, EventArgs e)
        {

        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                LoginBusiness business = new LoginBusiness();
                UsuarioDTO usuario = business.Logar(txtLogin.Text, txtSenha.Text);

                if (usuario != null)
                {
                    UserSession.UsuarioLogado = usuario;

                    if (usuario.Adiministrador == true)
                    {
                        frmMenuInicial menu = new frmMenuInicial();
                        menu.Show();
                    }
                    else
                    {
                        frmMenuInicial menu = new frmMenuInicial();
                        menu.Show();
                    }

                    this.Hide();
                }
                else
                {
                    MessageBox.Show("Credenciais inválidas.", "TheCloset", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "TheCloset",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreru um erro, tente mais tarde.", "TheCloset",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
    }
}
