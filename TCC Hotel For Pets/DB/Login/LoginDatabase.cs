﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TCC_Hotel_For_Pets.DB.BASE;

namespace TCC_Hotel_For_Pets.DB.Login
{
    class LoginDatabase
    {
        public UsuarioDTO Logar(string login, string senha)
        {
            string script = @"SELECT * FROM tb_Usuario WHERE ds_email_usuario = @ds_email_usuario AND ds_Senha_usuario = @ds_Senha_usuario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_email_usuario", login));
            parms.Add(new MySqlParameter("ds_Senha_usuario", senha));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            UsuarioDTO dto = null;

            if (reader.Read())
            {
                dto = new UsuarioDTO();
                dto.IdUsuario = reader.GetInt32("id_Usuario");
                dto.Nome = reader.GetString("nm_Usuario");
                dto.CPF = reader.GetString("ds_CPF");
                dto.Telefone = reader.GetString("ds_Telefone");
                dto.Celular = reader.GetString("ds_Celular");
                dto.EmailUsu = reader.GetString("ds_email_usuario");
                dto.SenhaUsu = reader.GetString("ds_Senha_usuario");
                dto.Adiministrador = reader.GetBoolean("bt_adm");
                dto.Funcionario = reader.GetBoolean("bt_funcionario");
            }
            reader.Close();

            return dto;
        }
    }
}
