﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCC_Hotel_For_Pets.DB.Estoque
{
    class EstoqueDTO
    {
        public int Id { get; set; }
        public string QtdProduto { get; set; }
        public decimal CustoAtual { get; set; }
        public decimal CustoTotal { get; set; }
        public int IdProduto { get; set; }
    }
}
