﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCC_Hotel_For_Pets.DB.Produto
{
    class ProdutoBusiness
    {
        public int Salvar(ProdutoDTO produto)
        {
            ProdutoDatabase produtoDB = new ProdutoDatabase();
            int id = produtoDB.Salvar(produto);
            return id;
        }
        public void Alterar(ProdutoDTO produto)
        {
            ProdutoDatabase produtoDB = new ProdutoDatabase();
            produtoDB.Alterar(produto);
        }
        public void Remover(int Id)
        {
            ProdutoDatabase produtoDB = new ProdutoDatabase();
            produtoDB.Remover(Id);

        }
        public List<ProdutoConsultarView> Consultar(string produto)
        {
            ProdutoDatabase db = new ProdutoDatabase();
            return db.Consultar(produto);
        }
        public List<ProdutoDTO> Listar()
        {
            ProdutoDatabase db = new ProdutoDatabase();
            return db.Listar();
        }
    }
}
