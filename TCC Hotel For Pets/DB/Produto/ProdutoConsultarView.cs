﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCC_Hotel_For_Pets.DB.Produto
{
    class ProdutoConsultarView
    {
            public int Id { get; set; }

            public string Nome { get; set; }

            public decimal Preco { get; set; }

            public string Fornecedor { get; set; }


        }
    
}
