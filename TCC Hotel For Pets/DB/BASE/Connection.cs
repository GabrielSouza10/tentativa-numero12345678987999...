﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCC_Hotel_For_Pets.DB.BASE
{
    class Connection
    {
        public MySqlConnection Create()
        {
            string connectionstring = "server=localhost;database=TheCloset;uid=root;password=1234;sslmode=none";

            MySqlConnection connection = new MySqlConnection(connectionstring);
            connection.Open();

            return connection;

        }
    }
}
