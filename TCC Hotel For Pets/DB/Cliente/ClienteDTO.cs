﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCC_Hotel_For_Pets.DB.Cliente
{
    class ClienteDTO
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public int CPF { get; set; }
        public string Bairro { get; set; }
        public int CEP { get; set; }
        public string Rua { get; set; }
        public string Telefone { get; set; }
        public int IdAnimal { get; set; }
        public int IdFuncionario { get; set; }
      
    }
}
