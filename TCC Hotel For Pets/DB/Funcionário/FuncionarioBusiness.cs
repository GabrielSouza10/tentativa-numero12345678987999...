﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCC_Hotel_For_Pets.DB.Funcionário
{
    class FuncionarioBusiness
    {
        public int Salvar(FuncionarioDTO produto)
        {
            FuncionarioDatabase produtoDB = new FuncionarioDatabase();
            int id = produtoDB.Salvar(produto);
            return id;
        }
        public void Alterar(FuncionarioDTO funcionario)
        {
            FuncionarioDatabase produtoDB = new FuncionarioDatabase();
            produtoDB.Alterar(funcionario);
        }

        public void Remover(int Id)
        {
            FuncionarioDatabase produtoDB = new FuncionarioDatabase();
            produtoDB.Remover(Id);

        }
        public List<FuncionarioConsultarView> Consultar(string funcionario)
        {
            FuncionarioDatabase db = new FuncionarioDatabase();
            return db.Consultar(funcionario);
        }
        public List<FuncionarioDTO> Listar()
        {
            FuncionarioDatabase db = new FuncionarioDatabase();
            return db.Listar();
        }
    }
}
