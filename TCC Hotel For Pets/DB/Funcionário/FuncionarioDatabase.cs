﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TCC_Hotel_For_Pets.DB.BASE;

namespace TCC_Hotel_For_Pets.DB.Funcionário
{
    class FuncionarioDatabase
    {
        public int Salvar(FuncionarioDTO funcionario)
        {
            string script =
            @"INSERT INTO tb_funcionario (id_funcionario , nm_nome , ds_cpf, ds_cargo, ds_senha, ds_data_de_nascimento, ds_telefone, ds_email, 
                           ds_rua, ds_bairro, ds_cep, nr_numero, ds_cidade, ds_estado, administrador_id_administrador, pagm_id_folha_de_pagamento)
                VALUES (@id_funcionario , @nm_nome , @ds_cpf,ds_cargo, @ds_senha, @ds_data_de_nascimento, @ds_telefone, @ds_email, @ds_rua, 
                           @ds_bairro, @ds_cep, @nr_numero, ds_cidade, @ds_estado, @administrador_id_administrador, @pagm_id_folha_de_pagamento)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_funcionario", funcionario.Id));
            parms.Add(new MySqlParameter("nm_nome", funcionario.Nome));
            parms.Add(new MySqlParameter("ds_cpf", funcionario.CPF));
            parms.Add(new MySqlParameter("ds_cargo", funcionario.Cargo));
            parms.Add(new MySqlParameter("ds_senha", funcionario.Senha));
            parms.Add(new MySqlParameter("ds_data_de_nascimento", funcionario.DataNasc));
            parms.Add(new MySqlParameter("ds_telefone", funcionario.Telefone));
            parms.Add(new MySqlParameter("ds_email", funcionario.Email));
            parms.Add(new MySqlParameter("ds_rua", funcionario.Rua));
            parms.Add(new MySqlParameter("ds_bairro", funcionario.Bairro));
            parms.Add(new MySqlParameter("ds_cep", funcionario.CEP));
            parms.Add(new MySqlParameter("nr_numero", funcionario.Numero));
            parms.Add(new MySqlParameter("ds_cidade", funcionario.Cidade));
            parms.Add(new MySqlParameter("ds_estado", funcionario.Estado));
            parms.Add(new MySqlParameter("administrador_id_administrador", funcionario.IdADM));
            parms.Add(new MySqlParameter("pagm_id_folha_de_pagamento", funcionario.IdFolhaPagm));

            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }
        public void Alterar(FuncionarioDTO funcionario)
        {
            string script =
            @"UPDATE tb_funcionario SET nm_nome = @nm_nome,
                WHERE id_funcionario = @id_funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_funcionario", funcionario.Id));
            parms.Add(new MySqlParameter("nm_nome", funcionario.Nome));
            parms.Add(new MySqlParameter("ds_cpf", funcionario.CPF));
            parms.Add(new MySqlParameter("ds_cargo", funcionario.Cargo));
            parms.Add(new MySqlParameter("ds_senha", funcionario.Senha));
            parms.Add(new MySqlParameter("ds_data_de_nascimento", funcionario.DataNasc));
            parms.Add(new MySqlParameter("ds_telefone", funcionario.Telefone));
            parms.Add(new MySqlParameter("ds_email", funcionario.Email));
            parms.Add(new MySqlParameter("ds_rua", funcionario.Rua));
            parms.Add(new MySqlParameter("ds_bairro", funcionario.Bairro));
            parms.Add(new MySqlParameter("ds_cep", funcionario.CEP));
            parms.Add(new MySqlParameter("nr_numero", funcionario.Numero));
            parms.Add(new MySqlParameter("ds_cidade", funcionario.Cidade));
            parms.Add(new MySqlParameter("ds_estado", funcionario.Estado));
            parms.Add(new MySqlParameter("administrador_id_administrador", funcionario.IdADM));
            parms.Add(new MySqlParameter("pagm_id_folha_de_pagamento", funcionario.IdFolhaPagm));
            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
        public void Remover(int idfunc)
        {
            string script =
            @"DELETE FROM tb_funcionario WHERE id_funcionario = @id_funcionario";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_funcionario", idfunc));
            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
        public List<FuncionarioDTO> Listar()
        {
            string script =
            @"SELECT * FROM tb_produto";
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, null);
            List<FuncionarioDTO> produtos = new List<FuncionarioDTO>();
            while (reader.Read())
            {
                FuncionarioDTO funcionario = new FuncionarioDTO();
                funcionario.Id = reader.GetInt32("id_funcionario");
                funcionario.Nome = reader.GetString("nm_nome");
                funcionario.CPF = reader.GetString("ds_cpf");
                funcionario.Cargo = reader.GetString("ds_cargo");
                funcionario.DataNasc = reader.GetDateTime("ds_data_de_nascimento");
                funcionario.Telefone = reader.GetString("ds_telefone");
                funcionario.Email = reader.GetString("ds_email");
                funcionario.CEP = reader.GetInt32("ds_cep");
                funcionario.IdFolhaPagm = reader.GetInt32("pagm_id_folha_de_pagamento");

                produtos.Add(funcionario);
            }
            reader.Close();
            return produtos;
        }
        public List<FuncionarioConsultarView> Consultar(string fornecedor)
        {
            string script = @"SELECT * FROM VW_FORNECEDOR_CONSULTA WHERE nm_nome like @nm_nome";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome", "%" + fornecedor + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FuncionarioConsultarView> lista = new List<FuncionarioConsultarView>();
            while (reader.Read())
            {
                FuncionarioConsultarView dto = new FuncionarioConsultarView();
                dto.Id = reader.GetInt32("id_funcionario");
                dto.Nome = reader.GetString("nm_nome");
                dto.Cpf = reader.GetString("ds_cpf");
                dto.Cargo = reader.GetString("ds_cargo");
                dto.Nascimento = reader.GetDateTime("ds_data_de_nascimento");
                dto.Telefone = reader.GetString("ds_telefone");
                dto.Email = reader.GetString("ds_email");
                dto.Cep = reader.GetInt32("ds_cep");
                dto.IdFolhaPagm = reader.GetInt32("pagm_id_folha_de_pagamento");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
    }
}
